#!/usr/bin/env python

numbers = raw_input("Enter numbers separated by spaces: ").strip().split()
numbers = [ int(a) for a in numbers ]
print "\n" + str(numbers)

i = 0
s = len(numbers)
while i < s - 1:
    j = i + 1
    min = numbers[i]
    toswap = False
    while j < s:
        if(numbers[j] < min):
            min = numbers[j]
            minidx = j
            toswap = True
        j += 1
    if toswap:
        numbers[i],numbers[minidx] = min,numbers[i]
        print numbers
    i += 1
