#!/usr/bin/env python

with open("/usr/share/dict/words") as words:
    for word in words:
        word = word.strip()
        w = len(word)
        if(w == 1):
            print(word + " is a palindrome")
            continue
        i = 0
        x = w/2
        pal = True
        while i < x :
            if word[i] == word[w-i-1]:
                i += 1
                continue
            else:
                pal = False
                break
        if pal:
            print(word + " is a palindrome")
