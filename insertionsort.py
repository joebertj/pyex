#!/usr/bin/env python

numbers = raw_input("Enter numbers separated by spaces: ").strip().split()
numbers = [ int(a) for a in numbers ]
print "\n" + str(numbers)

s = len(numbers)
i = 1
while i < s:
    j = 0
    while j < i:
        if(numbers[i] < numbers[j]):
            numbers[i],numbers[j] = numbers[j],numbers[i]
            print numbers
        j += 1
    i += 1
